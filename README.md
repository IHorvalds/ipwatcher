# IPWatcher

Watch your public IP with this container. Just run it every once in a while.

## Environment Variables

- WEB_TRACKER
- DNS_TRACKER
- DOMAIN_TO_TRACK
- CONTACT_EMAIL
- MAIL_SERVER
- MAIL_PORT
- MAIL_FROM
- MAIL_FROM_PASS
- ADMIN_MAIL
- CONTACT_EMAIL
- ENCRYPTION
- CF_AUTH_EMAIL
- CF_AUTH_KEY
- CF_DNS_ZONES

Only one of `WEB_TRACKER` and `DNS_TRACKER` is required (either-or). If both are 'true'-ish, DNS will be preferred. `WEB_TRACKER` is rate limited by the API and also needs `CONTACT_EMAIL` set in case they need to contact you regarding your usage. 

For mail, just use TLS. `ENCRYPTION` can be set to `STARTTLS`, anything else will assume SSL.

`CF_AUTH_EMAIL` and `CF_AUTH_KEY` are for the Cloudflare DNS updater. They are mandatory. You get them in your account page. The auth key is the General API key.
`CF_DNS_ZONES` is a list of Cloudlfare DNS zone identifiers in the format '["zone-id-1", "zone-id-2", .....]'.
