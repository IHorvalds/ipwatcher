import os
import logging
import globals

class OldIPGetter:
    
    @staticmethod
    def getOldIP():
        try:
            with open(os.path.join(globals.base_dir, globals.old_ip_file), "r") as old_ip_file:
                return old_ip_file.read().strip()
        except FileNotFoundError:
            logging.warning("Missing old IP file.")
            f = open(os.path.join(globals.base_dir, globals.old_ip_file), "w")
            f.write("")
            f.close()
        except Exception as e:
            logging.error(f"""Unknown error. Error message: {str(e)}""")