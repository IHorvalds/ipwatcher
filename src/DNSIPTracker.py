import logging
import socket
import os
import datetime

class DNSIPTracker:

    DOMAIN = os.environ['DOMAIN_TO_TRACK'] if 'DOMAIN_TO_TRACK' in os.environ else ''

    @staticmethod
    def getCurrentIP():
        if DNSIPTracker.DOMAIN == '':
            logging.error("""{} - No domain to track specified for DNS IP Tracker. Please set environment variable 'DOMAIN_TO_TRACK'""".format(datetime.datetime.now(datetime.timezone.utc)))
            exit(0)
        
        try:
            response = socket.gethostbyname(DNSIPTracker.DOMAIN)
            return response
        except Exception as e:
            logging.error("""{} - Error getting IP from DNS Request. Error: {}""".format(datetime.datetime.now(datetime.timezone.utc), str(e)))
            return None