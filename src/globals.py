import logging
import os
from jinja2 import Environment, FileSystemLoader, select_autoescape

logging.basicConfig(format="%(asctime)s - %(levelname)s %(message)s", datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)

base_path = os.path.dirname(os.path.abspath(__file__))

env = Environment(
    loader=FileSystemLoader(os.path.join(base_path, "templates")),
    autoescape=select_autoescape()
)

base_dir = "/old_ip_storage"
old_ip_file = "old_ip"
