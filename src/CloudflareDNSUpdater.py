import os
import json
import requests
import logging

class CloudflareDNSUpdater:

    base_url    = "https://api.cloudflare.com/client/v4/"
    zones       = os.environ['CF_DNS_ZONES'] if 'CF_DNS_ZONES' in os.environ else []
    unproxied   = os.environ['CF_UNPROXIED'] if 'CF_UNPROXIED' in os.environ else []

    headers = {
        "X-Auth-Email": os.environ['CF_AUTH_EMAIL'] if 'CF_AUTH_EMAIL' in os.environ else '',
        "X-Auth-Key": os.environ['CF_AUTH_KEY'] if 'CF_AUTH_KEY' in os.environ else '',
        "Content-Type": "application/json"
    }

    mail_template = "cf_update_template.html"

    @staticmethod
    def getRecordsForZone(zone):
        ## We already checked the headers and stuff
        endpoint = "zones/{zone_identifier}/dns_records"
        url = (CloudflareDNSUpdater.base_url + endpoint).format(
                    zone_identifier=zone
                )
        
        ## Log url, headers and data
        logging.info(f"""GET-ing to url {url}""")

        response = requests.get(
                    url,
                    headers=CloudflareDNSUpdater.headers
                )
        
        if not response.ok:
            logging.error(f"""Error calling url {url}""")

        response = response.json()

        if "success" in response and response["success"] == True:
            logging.info(f"""Got {len(response["result"])} zones""")
            return response["result"]
        
        return []

    @staticmethod
    def update(newIP):

        ## Each zone has multiple dns records (domain -> ip)
        ## Each record has in id. 
        ## To update the ip for all domains, 
        ## get all the records for each zone
        ## for each record, send a put request with the domain and new ip

        ## Sanity check.
        ## Verify we have all the information

        CloudflareDNSUpdater.zones = json.loads(CloudflareDNSUpdater.zones)
        CloudflareDNSUpdater.unproxied = json.loads(CloudflareDNSUpdater.unproxied)

        if len(CloudflareDNSUpdater.zones) == 0:
            ## Log empty zones.
            logging.error(f"""No zones! Please set environment variable CF_DNS_ZONES""")
            return

        if CloudflareDNSUpdater.headers["X-Auth-Email"] == '':
            ## Log no CF Auth Email
            logging.error(f"""Empty Auth Email! Please set environment variable CF_AUTH_EMAIL""")
            return

        if CloudflareDNSUpdater.headers["X-Auth-Key"] == '':
            ## Log no Auth key
            logging.error(f"""Empty Auth Key! Please set environment variable CF_AUTH_KEY""")
            return


        successful_updates = {}
        unsuccessful_updates = []

        endpoint = "zones/{zone_identifier}/dns_records/{identifier}"

        ## Get records for each zone
        for zone in CloudflareDNSUpdater.zones:
            records = CloudflareDNSUpdater.getRecordsForZone(zone)
            if len(records) == 0:
                ## No records?? Log this
                logging.error(f"""Empty zones! Please check if there are any records for zone {zone}""")
            
            ## Update
            for record in records:
                data = {
                    "type": "A",
                    "name": record["name"],
                    "content": newIP,
                    "ttl": "1",
                    "proxied": record["name"] not in CloudflareDNSUpdater.unproxied
                }
                url = (CloudflareDNSUpdater.base_url + endpoint).format(
                    zone_identifier=zone, 
                    identifier=record["id"]
                )

                ## Log url, headers and data
                logging.info(f"""PUT-ing to url {url};\ndata: {str(data)}""")

                response = requests.put(
                    url,
                    headers=CloudflareDNSUpdater.headers,
                    json=data
                )

                if not response.ok:
                    logging.error(f"""Error calling url {url}""")
                    break

                response = response.json()

                if "success" in response and response["success"] == True:
                    ## Log successful update for url with new IP.
                    ## Collect url and IP for mail.
                    logging.info(f"""Successful updating {record["name"]} with new IP {newIP}""")
                    proxied = record["name"] not in CloudflareDNSUpdater.unproxied
                    successful_updates[record["name"]] = (newIP, str(proxied))
                    

                else:
                    ## Log unsuccessful update.
                    ## Collection url for mail
                    logging.error(f"""Unuccessful updating {record["name"]} with new IP""")
                    unsuccessful_updates.append(record["name"])
        
        ## The keys in this dictionary **must** be the same as those in the mail template.
        ## They will be used to populate the template.
        ## Make sure to use the "cf_" namespace before the variables
        return CloudflareDNSUpdater.mail_template, {
            "cf_successful_updates": successful_updates,
            "cf_unsuccessful_updates": unsuccessful_updates
        }