import smtplib
from email.message import EmailMessage 
import os
import ssl
import logging
import globals
from functools import reduce

class MailService:

    mail_server     = os.environ['MAIL_SERVER']     if 'MAIL_SERVER'    in os.environ else ''
    port            = os.environ['MAIL_PORT']       if 'MAIL_PORT'      in os.environ else 465
    mail_from       = os.environ['MAIL_FROM']       if 'MAIL_FROM'      in os.environ else ''
    mail_from_pass  = os.environ['MAIL_FROM_PASS']  if 'MAIL_FROM_PASS' in os.environ else ''
    encryption      = os.environ['ENCRYPTION']      if 'ENCRYPTION'     in os.environ else ''

    context = ssl.SSLContext(ssl.PROTOCOL_TLS) if encryption.upper() == "STARTTLS" else ssl.create_default_context()

    mail_template = "mail_template.html"

    @staticmethod
    def getMailContent(contents):
        template = globals.env.get_template(MailService.mail_template)
        return template.render(contents)

    @staticmethod
    def sendMail(mail_to, subject, content, oldIP, newIP):

        if MailService.mail_server == "" or MailService.mail_from == "" or MailService.mail_from_pass == "":
            logging.error("Mail error. MAIL_SERVER, MAIL_FROM and MAIL_FROM_PASS environment variables must all be set.")
            return

        logging.info(f"""Preparing to send mail to {mail_to}.""")

        # Get mail contents
        templates, kwargs = zip(*content)
        templates = list(templates)
        kwargs = reduce(lambda m, n: {**m, **n}, list(kwargs)) ## merge all dictionaries from the templates together
        mail_content = MailService.getMailContent({"oldIP": oldIP, "newIP": newIP, "templates": templates, **kwargs})

        if MailService.encryption.upper() == "STARTTLS":
            try:
                with smtplib.SMTP(MailService.mail_server, MailService.port) as server:
                    MailService._send_mail(mail_to, subject, mail_content, server)
            except Exception as e:
                logging.error(f"""Unknown error: {str(e)}""")
        else:
            try:
                with smtplib.SMTP_SSL(MailService.mail_server, MailService.port, context=MailService.context) as server:
                    MailService._send_mail(mail_to, subject, mail_content, server)
            except Exception as e:
                logging.error(f"""Unknown error: {str(e)}""")


    @staticmethod
    def _send_mail(mail_to, subject, content, server):
        try:
            if MailService.encryption.upper() == "STARTTLS":
                server.starttls(context=MailService.context)
            server.login(MailService.mail_from, MailService.mail_from_pass)

            msg = EmailMessage()
            msg['Subject'] = subject
            msg['From'] = MailService.mail_from
            msg['To'] = mail_to
            msg.set_content(content, subtype="html")

            server.send_message(msg)
            logging.info(f"""Sent mail to {mail_to}""")
        except Exception as e:
            logging.error(f"""SMTP Error: {str(e)}""")