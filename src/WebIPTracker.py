import requests
import os
import re
import logging
import datetime

class WebIPTracker:
    
    ipBotAddress = "https://ipv4bot.whatismyipaddress.com/"
    ipv4Regex = "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"

    @staticmethod
    def getCurrentIP():
        try:
            headers = {
                'User-Agent': 'To contact me, please email {}'.format(os.environ['CONTACT_EMAIL'])
            }
        except KeyError:
            headers = {
                'User-Agent': ''
            }
        response = requests.get(WebIPTracker.ipBotAddress, headers=headers)

        if response.ok:
            ipv4 = response.text.strip()

            regex = re.fullmatch(WebIPTracker.ipv4Regex, ipv4)

            if regex is not None:
                return ipv4
            
            logging.error("""{} - Unexpected response from API. Response: {}""".format(datetime.datetime.now(datetime.timezone.utc)), ipv4)
            return None
        logging.error("""{} - Error from API. Status code returned: {}""".format(datetime.datetime.now(datetime.timezone.utc)), response.status_code)
        return None