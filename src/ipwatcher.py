import os
import globals
import logging

from mail import MailService
from WebIPTracker import WebIPTracker
from DNSIPTracker import DNSIPTracker
from GetOldIP import OldIPGetter
from CloudflareDNSUpdater import CloudflareDNSUpdater

def main():
    logging.info("Starting container.")
    DNS_TRACKER = os.environ['DNS_TRACKER'] if 'DNS_TRACKER' in os.environ else ""
    WEB_TRACKER = os.environ['WEB_TRACKER'] if 'WEB_TRACKER' in os.environ else ""

    admin_mail = os.environ['ADMIN_MAIL'] if 'ADMIN_MAIL' in os.environ else ""

    if admin_mail == "":
        logging.error("Missing ADMIN_MAIL environment variable.")
        exit(-1)

    currentIp = "E" ## Empty
    if DNS_TRACKER.lower() in ["true", "yes", "1"]:
        currentIp = DNSIPTracker.getCurrentIP()
    elif WEB_TRACKER.lower() in ["true", "yes", "1"]:
        currentIp = WebIPTracker.getCurrentIP()

    logging.info(f"{globals.getTime()} - Current ip is: {currentIp}")


    if currentIp == "E":
        logging.error("Please set one of DNS_TRACKER, WEB_TRACKER to true or 1.")
        exit(-1)

    oldIp = OldIPGetter.getOldIP()

    if currentIp != oldIp: ## IP has been changed
        if oldIp is None or oldIp == "":
            oldIp = "N/A"
            
        logging.info(f"""IP has changed from {oldIp} to {currentIp}""")

        with open(os.path.join(globals.base_dir, globals.old_ip_file), "w") as old_ip_file:

            ## (template, kwargs) template variables must have a unique namespace
            news = []

            ## Cloudflare Updater
            cf_template, updates  = CloudflareDNSUpdater.update(currentIp)
            news.append((cf_template, updates))


            MailService.sendMail(admin_mail, "Public IP Has Changed", news, oldIP=oldIp, newIP=currentIp)
    
            
            old_ip_file.write(currentIp)
            logging.info(f"""Written new IP {currentIp}""")
    else: ## IP is the same
        logging.info(f"""No change in IP {currentIp}""")
    
    logging.info("""End container""")

if __name__ == "__main__":
    main()